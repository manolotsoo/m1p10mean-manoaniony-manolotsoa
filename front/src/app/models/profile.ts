export class Profiles {
    firstName?: string;
    lastName?: string;
    address?: string;
    phoneNumber?: string;
    cin?: string;
    createdAt?: string;
    updatedAt?: string;
    constructor(init?: Partial<Profiles>) {
        Object.assign(this, init);
    }
}