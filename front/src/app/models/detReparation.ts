export class DetReparations {
    _id?: string;
    name?: string;
    unitPrice?: string;
    itemNumber?: string;
    payementState?: string;
    repState?: boolean;
    createdAt?: string;
    updatedAt?: string;
    releaseAt?: string;
    constructor(init?: Partial<DetReparations>) {
        Object.assign(this, init);
    }
}