export class Users {
    _id?: string;
    email?: string;
    userName?: string;
    password?: string;
    profile?: any;
    createdAt?: string;
    updatedAt?: string;
    userRoles?: string[];
    accessToken?: string;
    constructor(init?: Partial<Users>) {
        Object.assign(this, init);
    }
}