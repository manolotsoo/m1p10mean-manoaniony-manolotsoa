export class Cars {
    _id?: string;
    model?: string;
    mark?: string;
    registrationNumber?: string;
    image?: string;
    receiver?: string;
    user?: any;
    createdAt?: string;
    updatedAt?: string;
    detReparation?: [any];
    constructor(init?: Partial<Cars>) {
        Object.assign(this, init);
    }
}