import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminCarListComponent } from './dashboard-admin-car-list.component';

describe('DashboardClientCarListComponent', () => {
  let component: DashboardAdminCarListComponent;
  let fixture: ComponentFixture<DashboardAdminCarListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardAdminCarListComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DashboardAdminCarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
