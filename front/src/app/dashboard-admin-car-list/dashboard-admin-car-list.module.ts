import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardAdminCarListRoutingModule } from './dashboard-admin-car-list-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardAdminCarListRoutingModule
  ]
})
export class DashboardAdminCarListModule { }
