import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Cars } from '../models/car';
import { Users } from '../models/user';
import { AccountService, AuthenticationService, CarService } from '../services';

@Component({
  selector: 'app-dashboard-admin-car-list',
  templateUrl: './dashboard-admin-car-list.component.html',
  styleUrls: ['./dashboard-admin-car-list.component.css']
})
export class DashboardAdminCarListComponent {
  currentUser: Users | null;
  currentListCar: Cars[] = [];

  constructor(private router: Router,
    private accountService: AccountService,
    private authenticationService: AuthenticationService,
    private carService: CarService,
    private _snackBar: MatSnackBar,
  ) {
    this.currentUser = new Users();
  }
  ngOnInit() {
    this.currentUser = this.accountService.getUserValue();
    this.carService.currentUserCars().subscribe((cars: Cars[]) => {
      this.currentListCar = cars;
    })
  }

  openSnackBar(message: string, action: string, error?: boolean) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'bottom',
      panelClass: ['bg-error'],
      horizontalPosition: 'center'
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
  }

  onReceive(id: string) {
    this.carService.receiveCar(id).subscribe((response: any) => {
      if (response?.acknowledged) {
        this.openSnackBar('Voiture bien reçue', '');
        return window.location.reload();
      }
      else if (!response?.acknowledged) {
        return this.openSnackBar("Un erreur est survenue", '');
      }
    })
  }
}
