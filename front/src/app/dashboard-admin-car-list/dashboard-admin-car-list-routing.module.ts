import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardAdminCarListComponent } from './dashboard-admin-car-list.component';
const routes: Routes = [
    {
        path: 'dashboard-admin/car/list',
        component: DashboardAdminCarListComponent,
        data: { layout: Layouts.DashboardAdminCarList },
        // canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardAdminCarListRoutingModule { }