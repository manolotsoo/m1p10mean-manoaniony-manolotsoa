import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarViewComponent } from './dashboard-client-car-view.component';

describe('DashboardClientCarViewComponent', () => {
  let component: DashboardClientCarViewComponent;
  let fixture: ComponentFixture<DashboardClientCarViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardClientCarViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
