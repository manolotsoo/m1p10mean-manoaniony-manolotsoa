import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardClientCarViewComponent } from './dashboard-client-car-view.component';
const routes: Routes = [
    {
        path: 'dashboard/car/:id',
        component: DashboardClientCarViewComponent,
        data: { layout: Layouts.DashboardClientCarView },
        // canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardClientCarViewRoutingModule { }