import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardClientCarViewRoutingModule } from './dashboard-client-car-view-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        DashboardClientCarViewRoutingModule
    ]
})
export class DashboardClientCarViewModule { }
