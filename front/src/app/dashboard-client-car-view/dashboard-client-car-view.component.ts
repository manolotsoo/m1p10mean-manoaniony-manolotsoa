import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cars } from '../models/car';
import { CarService } from '../services';

@Component({
  selector: 'app-dashboard-client-car-view',
  templateUrl: './dashboard-client-car-view.component.html',
  styleUrls: ['./dashboard-client-car-view.component.css']
})
export class DashboardClientCarViewComponent {
  idCar: string | null = null;
  carRetrieved: Cars = new Cars();
  noDetail: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private carService: CarService
  ) {
  }

  ngOnInit() {
    this.idCar = this.route.snapshot.paramMap.get('id');
    this.carService.getCar(this.idCar).subscribe((response: Cars) => {
      this.carRetrieved = response;
      console.log(`response?.detReparation`);
      console.log(response?.detReparation);
    })
  }
}
