import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminCarListLayoutComponent } from './dashboard-admin-car-list-layout.component';

describe('DashboardAdminCarListLayoutComponent', () => {
  let component: DashboardAdminCarListLayoutComponent;
  let fixture: ComponentFixture<DashboardAdminCarListLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardAdminCarListLayoutComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DashboardAdminCarListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
