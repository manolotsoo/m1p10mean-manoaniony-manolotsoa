import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarViewLayoutComponent } from './dashboard-client-car-view-layout.component';

describe('DashboardClientCarViewLayoutComponent', () => {
  let component: DashboardClientCarViewLayoutComponent;
  let fixture: ComponentFixture<DashboardClientCarViewLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardClientCarViewLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarViewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
