import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminCarViewLayoutComponent } from './dashboard-admin-car-view-layout.component';

describe('DashboardAdminCarViewLayoutComponent', () => {
  let component: DashboardAdminCarViewLayoutComponent;
  let fixture: ComponentFixture<DashboardAdminCarViewLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardAdminCarViewLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardAdminCarViewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
