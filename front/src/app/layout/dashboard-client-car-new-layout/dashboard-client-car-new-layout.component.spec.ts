import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarNewLayoutComponent } from './dashboard-client-car-new-layout.component';

describe('DashboardClientCarNewLayoutComponent', () => {
  let component: DashboardClientCarNewLayoutComponent;
  let fixture: ComponentFixture<DashboardClientCarNewLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardClientCarNewLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarNewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
