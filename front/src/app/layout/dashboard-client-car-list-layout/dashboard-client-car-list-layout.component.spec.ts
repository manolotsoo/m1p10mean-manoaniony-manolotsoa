import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarListLayoutComponent } from './dashboard-client-car-list-layout.component';

describe('DashboardClientCarListLayoutComponent', () => {
  let component: DashboardClientCarListLayoutComponent;
  let fixture: ComponentFixture<DashboardClientCarListLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardClientCarListLayoutComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
