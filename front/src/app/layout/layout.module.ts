import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardClientCarListLayoutComponent } from './dashboard-client-car-list-layout/dashboard-client-car-list-layout.component';
import { DashboardAdminCarListLayoutComponent } from './dashboard-admin-car-list-layout/dashboard-admin-car-list-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { RegisterLayoutComponent } from './register-layout/register-layout.component';
import { DashboardAdminLayoutComponent } from './dashboard-admin-layout/dashboard-admin-layout.component';
import { DashboardClientCarNewLayoutComponent } from './dashboard-client-car-new-layout/dashboard-client-car-new-layout.component';
import { DashboardClientCarViewLayoutComponent } from './dashboard-client-car-view-layout/dashboard-client-car-view-layout.component';
import { DashboardAdminCarViewLayoutComponent } from './dashboard-admin-car-view-layout/dashboard-admin-car-view-layout.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([]),
        FlexLayoutModule,
    ],
    exports: [
        DashboardAdminLayoutComponent,
        DashboardClientCarListLayoutComponent,
        DashboardAdminCarListLayoutComponent,
        LoginLayoutComponent,
        RegisterLayoutComponent,
        DashboardClientCarNewLayoutComponent,
        DashboardClientCarViewLayoutComponent,
        DashboardAdminCarViewLayoutComponent
    ],
    declarations: [
        DashboardAdminLayoutComponent,
        DashboardClientCarListLayoutComponent,
        DashboardAdminCarListLayoutComponent,
        LoginLayoutComponent,
        RegisterLayoutComponent,
        DashboardClientCarNewLayoutComponent,
        DashboardClientCarViewLayoutComponent,
        DashboardAdminCarViewLayoutComponent
    ],
})
export class LayoutModule { }