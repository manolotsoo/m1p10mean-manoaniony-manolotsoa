import { Component } from '@angular/core';

@Component({
  selector: 'app-register-layout',
  templateUrl: './register-layout.component.html',
  styleUrls: ['./register-layout.component.css']
})
export class RegisterLayoutComponent {
  constructor() {
    // this.loadScripts();
  }

  loadScripts() {
    const dynamicScripts = [
      'src/assets/regiter/plugins/jquery/jquery.min.js',
      'src/assets/regiter/plugins/bootstrap/js/bootstrap.bundle.min.js',
      'src/assets/regiter/dist/js/adminlte.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('body')[0].appendChild(node);
    }
  }
}
