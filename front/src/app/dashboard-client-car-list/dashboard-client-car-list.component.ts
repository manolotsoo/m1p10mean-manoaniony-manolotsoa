import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Cars } from '../models/car';
import { Users } from '../models/user';
import { AccountService, AuthenticationService, CarService } from '../services';

@Component({
  selector: 'app-dashboard-client-car-list',
  templateUrl: './dashboard-client-car-list.component.html',
  styleUrls: ['./dashboard-client-car-list.component.css']
})
export class DashboardClientCarListComponent {
  currentUser: Users | null;
  currentListCar: Cars[] = [];

  constructor(private router: Router,
    private accountService: AccountService,
    private authenticationService: AuthenticationService,
    private carService: CarService
  ) {
    this.currentUser = new Users();
  }
  ngOnInit() {
    this.currentUser = this.accountService.getUserValue();
    this.carService.currentUserCars().subscribe((cars: Cars[]) => {
      this.currentListCar = cars;
    })
  }

  goToCarView(_id: string | undefined) {
    this.router.navigateByUrl('/dashboard/car/view/_id');
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
  }
}
