import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardClientCarListComponent } from './dashboard-client-car-list.component';
const routes: Routes = [
    {
        path: 'dashboard/car/list',
        component: DashboardClientCarListComponent,
        data: { layout: Layouts.DashboardClientCarList },
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardClientCarListRoutingModule { }