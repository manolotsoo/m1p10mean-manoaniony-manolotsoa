import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarListComponent } from './dashboard-client-car-list.component';

describe('DashboardClientCarListComponent', () => {
  let component: DashboardClientCarListComponent;
  let fixture: ComponentFixture<DashboardClientCarListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardClientCarListComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
