import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardClientCarListRoutingModule } from './dashboard-client-car-list-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardClientCarListRoutingModule
  ]
})
export class DashboardClientCarListModule { }
