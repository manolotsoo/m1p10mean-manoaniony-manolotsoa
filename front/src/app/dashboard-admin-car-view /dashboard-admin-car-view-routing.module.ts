import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardAdminCarViewComponent } from './dashboard-admin-car-view.component';
const routes: Routes = [
    {
        path: 'dashboard-admin/car/:id',
        component: DashboardAdminCarViewComponent,
        data: { layout: Layouts.DashboardClientCarView },
        // canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardAdminCarViewRoutingModule { }