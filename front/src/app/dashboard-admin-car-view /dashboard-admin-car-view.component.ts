import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Cars } from '../models/car';
import { DetReparations } from '../models/detReparation';
import { CarService } from '../services';

@Component({
  selector: 'app-dashboard-admin-car-view',
  templateUrl: './dashboard-admin-car-view.component.html',
  styleUrls: ['./dashboard-admin-car-view.component.css']
})
export class DashboardAdminCarViewComponent {
  idCar: string | null = null;
  carRetrieved: Cars = new Cars();
  noDetail: boolean = false;
  formReparation!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.idCar = this.route.snapshot.paramMap.get('id');
    this.carService.getCar(this.idCar).subscribe((response: Cars) => {
      this.carRetrieved = response;
    });
    this.formReparation = new FormGroup({
      name: new FormControl('test name', [Validators.required]),
      unitPrice: new FormControl(12000, [Validators.required, Validators.min(0)]),
      itemNumber: new FormControl(2, [Validators.required, Validators.min(0)]),
      paymentStatus: new FormControl(true, [])
    })
    console.log(`VALIDITY`);
    console.log(this.formReparation);
  }

  getNameErrorMessage() {
    if (this.formReparation.get('name')?.touched && this.formReparation.get('name')?.hasError('required')) {
      return 'Vous devez entrer le libellé de votre detail reparation';
    }
    return this.formReparation.get('name')?.hasError('name')
      ? 'Vous devez entrer un libelé valide'
      : '';
  }

  getUnitPriceErrorMessage() {
    if (this.formReparation.get('unitPrice')?.touched && this.formReparation.get('unitPrice')?.hasError('required')) {
      return 'Vous devez entrer le prix unitaire de votre detail reparation';
    }
    return this.formReparation.get('unitPrice')?.hasError('min')
      ? 'Vous devez entrer un prix unitaire valide'
      : '';
  }

  getItemNumberErrorMessage() {
    console.log(this.formReparation.get('itemNumber')?.touched && this.formReparation.get('itemNumber'));
    if (this.formReparation.get('itemNumber')?.touched && this.formReparation.get('itemNumber')?.hasError('required')) {
      return 'Vous devez entrer un nombre';
    }
    return this.formReparation.get('itemNumber')?.hasError('min')
      ? 'Vous devez entrer un nombre valide'
      : '';
  }

  openSnackBar(message: string, action: string, error?: boolean) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      panelClass: ['bg-error'],
      horizontalPosition: 'right'
    });
  }

  onSubmit() {
    if (this.formReparation.valid) {
      const { name, unitPrice, itemNumber, paymentStatus } = this.formReparation?.value;
      alert(JSON.stringify({ name, unitPrice, itemNumber, paymentStatus }));
      const varDetReparation = new DetReparations({ name, unitPrice, itemNumber, payementState: paymentStatus });
      this.carService.addDetReparation(this.idCar!, varDetReparation).subscribe((response: any) => {
        if (response?.acknowledged) {
          this.openSnackBar('Detail réparation bien ajouté', 'reussi');
          return window.location.reload();
        }
        else if (!response?.acknowledged) {
          return this.openSnackBar("", 'Un erreur est survenue');
        }
      })
    }
  }
}
