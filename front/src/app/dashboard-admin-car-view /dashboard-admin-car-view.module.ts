import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardAdminCarViewRoutingModule } from './dashboard-admin-car-view-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardAdminCarViewComponent } from './dashboard-admin-car-view.component';
import { MaterialModule } from '../material-modules';

@NgModule({
    declarations: [DashboardAdminCarViewComponent],
    imports: [
        CommonModule,
        DashboardAdminCarViewRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule
    ]
})
export class DashboardAdminCarViewModule { }
