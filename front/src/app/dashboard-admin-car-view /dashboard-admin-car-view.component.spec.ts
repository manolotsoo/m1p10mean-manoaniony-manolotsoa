import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminCarViewComponent } from './dashboard-admin-car-view.component';

describe('DashboardAdminCarViewComponent', () => {
  let component: DashboardAdminCarViewComponent;
  let fixture: ComponentFixture<DashboardAdminCarViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardAdminCarViewComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DashboardAdminCarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
