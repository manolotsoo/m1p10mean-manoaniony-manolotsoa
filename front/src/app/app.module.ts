import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { LayoutModule } from './layout/layout.module';
import { MaterialModule } from './material-modules';
import { RegisterModule } from './register/register.module';
import { DashboardClientCarListModule } from './dashboard-client-car-list/dashboard-client-car-list.module';
import { DashboardClientCarNewModule } from './dashboard-client-car-new/dashboard-client-car-new.module';
import { DashboardClientCarViewModule } from './dashboard-client-car-view/dashboard-client-car-view.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helper/jwt.interceptor';
import { AuthGuard, ErrorInterceptor } from './helper';
import { DashboardAdminModule } from './dashboard-admin/dashboard-admin.module';
import { AccountService } from "./services";
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardClientCarListComponent } from './dashboard-client-car-list/dashboard-client-car-list.component';
import { DashboardClientCarViewComponent } from './dashboard-client-car-view/dashboard-client-car-view.component';
import { DashboardAdminCarListComponent } from './dashboard-admin-car-list/dashboard-admin-car-list.component';
import { DashboardAdminCarListModule } from './dashboard-admin-car-list/dashboard-admin-car-list.module';
import { DashboardAdminCarViewComponent } from './dashboard-admin-car-view /dashboard-admin-car-view.component';
import { DashboardAdminCarViewModule } from './dashboard-admin-car-view /dashboard-admin-car-view.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardAdminComponent,
    DashboardClientCarListComponent,
    DashboardClientCarViewComponent,
    DashboardAdminCarListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    RegisterModule,
    DashboardClientCarListModule,
    DashboardClientCarNewModule,
    DashboardClientCarViewModule,
    DashboardAdminCarListModule,
    DashboardAdminCarViewModule,
    DashboardAdminModule,
    LayoutModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    AccountService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
