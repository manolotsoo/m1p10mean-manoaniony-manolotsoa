import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Profiles } from '../models/profile';
import { Users } from '../models/user';
import { AuthenticationService } from '../services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  loading: boolean = false;
  submitted: boolean = false;
  registerForm!: FormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      lastName: new FormControl('Razafindrakoto', [Validators.required]),
      firstName: new FormControl('Manolotsoa', [Validators.required]),
      address: new FormControl('Lot II A B 22', [Validators.required]),
      phoneNumber: new FormControl('+261324001027', [Validators.required]),
      cin: new FormControl('102031023548', [Validators.required]),
      email: new FormControl('manolotsoadaniel@gmail.com', [Validators.required, Validators.email]),
      password: new FormControl('itu1506!', [
        Validators.required,
        Validators.minLength(8)
      ]),
      confirmPassword: new FormControl('itu1506!', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
    this.registerForm.setValidators(this.matchValidator());
  }

  getLastNameErrorMessage() {
    if (this.registerForm.get('lastName')?.touched && this.registerForm.get('lastName')?.hasError('required')) {
      return 'Vous devez entrer votre nom';
    }
    return this.registerForm.get('lastName')?.hasError('lastName')
      ? 'Vous devez entrer un nom valide'
      : '';
  }

  getFirstNameErrorMessage() {
    if (this.registerForm.get('firstName')?.touched && this.registerForm.get('firstName')?.hasError('required')) {
      return 'Vous devez entrer votre prénom';
    }
    return this.registerForm.get('firsName')?.hasError('firstName')
      ? 'Vous devez entrer un prénom valide'
      : '';
  }

  getAddressErrorMessage() {
    if (this.registerForm.get('address')?.touched && this.registerForm.get('address')?.hasError('required')) {
      return 'Vous devez entrer votre adresse';
    }
    return this.registerForm.get('address')?.hasError('address')
      ? 'Vous devez entrer une adresse valide'
      : '';
  }

  getPhoneNumberErrorMessage() {
    if (this.registerForm.get('phoneNumber')?.touched && this.registerForm.get('phoneNumber')?.hasError('required')) {
      return 'Vous devez entrer votre numéro téléphone';
    }
    return this.registerForm.get('phoneNumber')?.hasError('phoneNumber')
      ? 'Vous devez entrer un numéro téléphone valide'
      : '';
  }

  getCinErrorMessage() {
    if (this.registerForm.get('cin')?.touched && this.registerForm.get('cin')?.hasError('required')) {
      return 'Vous devez entrer votre CIN';
    }
    return this.registerForm.get('cin')?.hasError('cin')
      ? 'Vous devez entrer un numéro CIN valide'
      : '';
  }

  getEmailErrorMessage() {
    if (this.registerForm.get('email')?.touched && this.registerForm.get('email')?.hasError('required')) {
      return 'Vous devez entrer votre email';
    }
    return this.registerForm.get('email')?.hasError('email')
      ? 'Vous devez entrer un adresse email valide'
      : '';
  }

  getPasswordErrorMessage() {
    if (this.registerForm.get('password')?.touched && this.registerForm.get('password')?.hasError('required')) {
      return 'Vous devez entrer votre mot de passe';
    }
    return this.registerForm.get('password')?.hasError('password')
      ? 'Vous devez entrer un mot de passe valide'
      : '';
  }

  getConfirmPasswordErrorMessage() {
    // if (this.registerForm.get('confirmPassword')?.touched && this.registerForm.get('confirmPassword')?.hasError('required')) {
    //   return 'Vous devez entrer votre confirmation mot de passe';
    // }
    return this.registerForm.get('confirmPassword')?.touched && this.registerForm.get('confirmPassword')?.hasError('required') || this.registerForm.get('confirmPassword')?.hasError('confirmPassword') || this.registerForm.errors
      ? 'Mot de passe non identique'
      : '';
  }

  matchValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const { password, confirmPassword } = control.value;
      return password !== confirmPassword
        ? { mismatch: true }
        : null;
    };
  }

  openSnackBar(message: string, action: string, error?: boolean) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      panelClass: ['bg-error'],
      horizontalPosition: 'right'
    });
  }

  onSubmit() {
    const {
      lastName,
      firstName,
      address,
      phoneNumber,
      cin,
      email,
      password
    } = this.registerForm?.value;
    const profile = new Profiles({
      firstName,
      lastName,
      address,
      phoneNumber,
      cin
    });
    const userRoles = ['CUSTOMER']
    const user = new Users({
      email,
      password,
      profile,
      userRoles,
    });
    if (this.registerForm.valid) {
      this.authenticationService.register(user).subscribe((response: any) => {
        if (response?.acknowledged) {
          this.openSnackBar('Utilisateur bien inscrit', 'inscription reussi');
          return this.router.navigateByUrl('/login');
        }
        else if (!response?.acknowledged) {
          return this.openSnackBar("L'email n'est plus disponible", 'Un erreur est survenue');
        }
      })
    }

  }

}
