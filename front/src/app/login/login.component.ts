import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService, AuthenticationService } from '../services';
import jwtDecode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loading: boolean = false;
  submitted: boolean = false;
  loginForm!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private accountService: AccountService,
  ) {
  }

  getEmailErrorMessage() {
    if (this.loginForm.get('email')?.hasError('required')) {
      return 'You must enter a email';
    }

    return this.loginForm.get('email')?.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  getPasswordErrorMessage() {
    if (this.loginForm.get('password')?.hasError('required')) {
      return 'You must enter a password';
    }

    return this.loginForm.get('password')?.hasError('password')
      ? 'Not a valid password'
      : '';
  }
  onSubmit(): void {
    const { email, password } = this.loginForm?.value;
    this.authenticationService.login(email, password)
      .subscribe(
        {
          next: (value) => {
            localStorage.setItem('accessToken', value as string);
            // get return url from query parameters or default to home page
            // const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
            const userConnected = this.accountService.getUserValue();
            console.log(userConnected);
            let returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard/car/list';
            if (userConnected?.userRoles?.includes("WORKSHOP_RESPONSABLE")) {
              returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard-admin/car/list';
            }
            return this.router.navigateByUrl(returnUrl);
          },
          error: error => {
            alert(JSON.stringify(error))
            this.loading = false;
          }
        }
      );

  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('manolotsoadaniel@gmail.com', [Validators.required, Validators.email]),
      password: new FormControl('itu1506!', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
    //   this.form = this.formBuilder.group({
    //     username: ['', Validators.required],
    //     password: ['', Validators.required]
    // });
  }
}