import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardClientCarNewComponent } from './dashboard-client-car-new.component';
const routes: Routes = [
    {
        path: 'dashboard/car/new',
        component: DashboardClientCarNewComponent,
        data: { layout: Layouts.DashboardClientCarNew },
        // canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardClientCarNewRoutingModule { }