import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardClientCarNewRoutingModule } from './dashboard-client-car-new-routing.module';
import { MaterialModule } from '../material-modules';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardClientCarNewComponent } from './dashboard-client-car-new.component';

@NgModule({
    declarations: [DashboardClientCarNewComponent],
    imports: [
        CommonModule,
        DashboardClientCarNewRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
    ]
})
export class DashboardClientCarNewModule { }
