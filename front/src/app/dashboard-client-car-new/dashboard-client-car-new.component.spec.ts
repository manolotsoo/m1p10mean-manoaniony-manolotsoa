import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientCarNewComponent } from './dashboard-client-car-new.component';

describe('DashboardClientCarNewComponent', () => {
  let component: DashboardClientCarNewComponent;
  let fixture: ComponentFixture<DashboardClientCarNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardClientCarNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardClientCarNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
