import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Cars } from '../models/car';
import { AuthenticationService, CarService } from '../services';

@Component({
  selector: 'app-dashboard-client-car-new',
  templateUrl: './dashboard-client-car-new.component.html',
  styleUrls: ['./dashboard-client-car-new.component.css']
})
export class DashboardClientCarNewComponent {
  loading: boolean = false;
  newCarForm!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar,
    private authenticationService: AuthenticationService,
    private carService: CarService,
  ) { }

  ngOnInit(): void {
    this.newCarForm = new FormGroup({
      model: new FormControl('Golf', [Validators.required]),
      mark: new FormControl('Volkswagen', [Validators.required]),
      registrationNumber: new FormControl('1641TBD', [Validators.required])
    })
  }

  getModelErrorMessage() {
    if (this.newCarForm.get('model')?.touched && this.newCarForm.get('model')?.hasError('required')) {
      return 'Vous devez entrer le modèle de voiture';
    }
    return this.newCarForm.get('model')?.hasError('model')
      ? 'Vous devez entrer un modele valide'
      : '';
  }

  getMarkErrorMessage() {
    if (this.newCarForm.get('mark')?.touched && this.newCarForm.get('mark')?.hasError('required')) {
      return 'Vous devez entrer la marque de voiture';
    }
    return this.newCarForm.get('mark')?.hasError('mark')
      ? 'Vous devez entrer une marque voiture valide'
      : '';
  }

  getRegistrationNumberErrorMessage() {
    if (this.newCarForm.get('registrationNumber')?.touched && this.newCarForm.get('registrationNumber')?.hasError('required')) {
      return 'Vous devez entrer la matricule de la voiture';
    }
    return this.newCarForm.get('registrationNumber')?.hasError('registrationNumber')
      ? 'Vous devez entrer une matricule de voiture valide'
      : '';
  }

  openSnackBar(message: string, action: string, error?: boolean) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'bottom',
      panelClass: ['bg-error'],
      horizontalPosition: 'center'
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
  }

  onSubmit() {
    const {
      model,
      mark,
      registrationNumber
    } = this.newCarForm?.value;
    const newCar = new Cars({
      model,
      mark,
      registrationNumber
    });
    if (this.newCarForm.valid) {
      this.carService.registration(newCar).subscribe((response: any) => {
        console.log(`RESPONSE`);
        console.log(response);
        if (response?.acknowledged) {
          this.openSnackBar('Formulaire de voiture envoyé', 'formulaire de depot reussi');
          return this.router.navigateByUrl('/dashboard/car/list');
        }
        else if (!response?.acknowledged) {
          return this.openSnackBar("Il y a une erreur lors de l'envoi du formulaire", '');
        }
      })
    }
  }

  onReset() {
    this.newCarForm.reset();
  }

}
