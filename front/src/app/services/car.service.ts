import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Cars } from '../models/car';
import { AccountService } from './account.service';
import { environment as environmentDev } from 'src/environments/environment';
import { DetReparations } from '../models/detReparation';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private accountService: AccountService
  ) { }

  registration(car: Cars) {
    const { model, mark, registrationNumber } = car;
    return this.http.post<Cars>(`${environmentDev.apiUrl}/car/add`, {
      model,
      mark,
      registrationNumber,
    })
  }

  currentUserCars() {
    return this.http.get<Cars[]>(`${environmentDev.apiUrl}/car/getCarCurrentUser`);
  }

  getCar(id: string | null) {
    return this.http.get<Cars>(`${environmentDev.apiUrl}/car/getCar/${id}`);
  }

  receiveCar(id: string) {
    return this.http.post<any>(`${environmentDev.apiUrl}/car/receive/${id}`, null)
  }

  addDetReparation(id: string, detReparation: DetReparations) {
    return this.http.post<any>(`${environmentDev.apiUrl}/car/ajoutDetReparation/${id}`, { detReparation })
  }
}
