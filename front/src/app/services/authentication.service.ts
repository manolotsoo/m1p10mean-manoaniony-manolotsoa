import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from '../models/user';
import { environment as environmentDev } from 'src/environments/environment';
import { map } from 'rxjs';
import { AccountService } from './account.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private accountService: AccountService
  ) { }

  login(email: string, password: string) {
    return this.http.post<Users>(`${environmentDev.apiUrl}/auth/login`, { email, password })
      .pipe(map(user => {
        const { accessToken } = user;
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        // this.accountService.setTokenValue(accessToken as string);
        // this.accountService.setUserValue(user as Users);

        return accessToken;
      }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('accessToken');
    this.router.navigate(['/login']);
  }

  register(user: Users) {
    const { profile, email, password } = user;
    const usersRoles = ['CUSTOMER'];
    return this.http.post<Users>(`${environmentDev.apiUrl}/users/ajoutUsers`, {
      profile,
      email,
      password,
      usersRoles
    })
  }


}
