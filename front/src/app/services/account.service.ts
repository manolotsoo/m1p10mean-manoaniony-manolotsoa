import { HttpClient } from '@angular/common/http';
import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { Users } from '../models/user';
import { environment as environmentDev } from 'src/environments/environment';
import jwtDecode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private tokenSubject: BehaviorSubject<string | null> = new BehaviorSubject(localStorage.getItem('accessToken'));
  private userSubject: BehaviorSubject<Users | null> = new BehaviorSubject<Users | null>(null);
  currentUser: Users = new Users();
  constructor(
    private router: Router,
    private http: HttpClient,
    @Optional() @SkipSelf() accountService?: AccountService) {
    if (accountService) {
      throw new Error("AccountService is already loaded");
    }
    console.info('Auth Service created');
  }

  public getTokenValue() {
    return this.tokenSubject.value as string;
  }

  public setTokenValue(token: string) {
    this.tokenSubject.next(token);
  }

  public setUserValue(user: Users) {
    this.userSubject.next(user);
  }
  public getUserValue() {
    if (localStorage.getItem('accessToken')) {
      const token = localStorage.getItem('accessToken') || '';
      return jwtDecode(token) ? new Users(jwtDecode(token)) : null;
    }
    return this.userSubject.value as Users;
  }


  // currentUser() {
  //   const accessToken = localStorage.getItem('accessToken');
  //   if (accessToken) {
  //     return this.http.get<any>(`${environmentDev.apiUrl}/auth/me`,
  //       {
  //         headers:
  //         {
  //           authorization: `Bearer ${accessToken}`
  //         }
  //       }).pipe(map(user => {
  //         // store user details and jwt token in local storage to keep user logged in between page refreshes
  //         this.userSubject?.next(user);
  //         return user;
  //       }));
  //   }
  //   return null;
  // }

  // login(email: string, password: string) {
  //   return this.http.post<Users>(`${environmentDev.apiUrl}/auth/login`, { email, password })
  //     .pipe(map(user => {
  //       const { accessToken } = user;
  //       // store user details and jwt token in local storage to keep user logged in between page refreshes
  //       this.tokenSubject.next(accessToken as string);
  //       this.userSubject.next(user);
  //       return accessToken;
  //     }));
  // }

  // logout() {
  //   // remove user from local storage and set current user to null
  //   localStorage.removeItem('accessToken');
  //   this.router.navigate(['/login']);
  // }
}
