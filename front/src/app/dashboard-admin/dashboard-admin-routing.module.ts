import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Layouts } from '../app.component';
import { AuthGuard } from '../helper';
import { DashboardAdminComponent } from './dashboard-admin.component';
const routes: Routes = [
    {
        path: 'dashboard-admin',
        component: DashboardAdminComponent,
        data: { layout: Layouts.DashboardAdmin },
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardAdminRoutingModule { }