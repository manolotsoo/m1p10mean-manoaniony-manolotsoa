import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Users } from '../models/user';
import { AccountService } from '../services';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css']
})
export class DashboardAdminComponent {
  currentUser: Users | null;

  constructor(private router: Router, private accountService: AccountService) {
    this.currentUser = new Users();
  }
  ngOnInit() {
    this.currentUser = this.accountService.currentUser
  }
}
