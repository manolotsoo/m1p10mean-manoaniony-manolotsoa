class Car {
    constructor({_id,model,mark,registrationNumber,user,receive,receiver,createdAt,updatedAt,releaseAt,detReparation,image}) {
        this._id = _id,
        this.model = model;
        this.mark = mark;
        this.registrationNumber = registrationNumber;
        this.user = user;
        this.image = image;
        this.receive = receive;
        this.receiver = receiver;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.releaseAt = releaseAt;
        this.detReparation = detReparation;
      }
    }
    module.exports = { Car };