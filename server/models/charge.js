class Charge {
    constructor({type,amount,createdAt,updatedAt,payementState}) {
        this.type = type,
        this.amount = amount,
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.payementState = payementState;
      }
    }
    module.exports = { Charge };