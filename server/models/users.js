class Users {
  constructor({_id,email,userName,password,profile,createdAt,updatedAt,usersRoles,accessToken}) {
      this._id = _id,
      this.email = email;
      this.userName = userName;
      this.password = password;
      this.profile = profile;
      this.createdAt = createdAt;
      this.updatedAt = updatedAt;
      this.usersRoles = usersRoles;
      this.accessToken=accessToken;
    }
  }
  module.exports = { Users };
  