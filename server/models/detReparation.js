class DetReparation {
    constructor({name,unitPrice,itemNumber,payementState,createdAt,updatedAt,releaseAt,repState}) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.itemNumber = itemNumber;
        this.payementState = payementState;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.releaseAt = releaseAt;
        this.repState = repState
      }
    }
    module.exports = { DetReparation };