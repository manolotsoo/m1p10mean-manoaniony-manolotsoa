const express = require('express');
const { initConstraintDB } = require("./db/connect");
const routerUsers = require("./routes/usersRoute");
const routerVoiture = require("./routes/voitureRoute");
const routerAuth = require("./routes/authRoute");
const routerCharge = require("./routes/chargeRoute");
const cors = require('cors');
const app = express();
const config = require("./config");
const {mongoDb} = config;
require('dotenv').config()

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors({
  origin: '*',
  //optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

app.use("/users", routerUsers);
app.use("/car", routerVoiture);
app.use("/auth", routerAuth);
app.use("/charge", routerCharge);

//const connectionString = "mongodb://127.0.0.1:27017/";
//myFirstDatabase?appName=mongosh+1.3.1
let connectionString = "mongodb+srv://admin:admin@clustergarage.ud9nxz4.mongodb.net/";
if (process.env.NODE_ENV==="development") {
  connectionString = `${mongoDb.url}/${mongoDb.databaseName}`;
  console.log(connectionString);
}
else{
  connectionString = "mongodb+srv://admin:admin@clustergarage.ud9nxz4.mongodb.net/";
}
initConstraintDB();
// connect(connectionString, (err) => {
//   if (err) {
//     console.log("Erreur lors de la connexion à la base de données");
//     console.log(err);
//     process.exit(-1);
//   } else {
//     console.log("Connexion avec la base de données établie");
//     console.log("Attente des requêtes au port 3OOO");
//   }
// });
app.listen(process.env.PORT || 3000);