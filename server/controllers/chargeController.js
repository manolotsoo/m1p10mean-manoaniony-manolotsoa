const { ObjectID } = require("bson");
const client = require("../db/connect");
const moment = require('moment');
const { Charge } = require("../models/charge");
const { TypeCharge } = require("../models/typeCharge");



const ajoutCharge = async (req, res) => {
    try {
      let data = req.body;
      let type  = new TypeCharge(data.type);
      let charge = new Charge({
        type : type,
        amount : data.amount,
        createdAt : moment().format("DD/MM/YYYY HH:mm"),
        updatedAt : moment().format("DD/MM/YYYY HH:mm"),
        payementState : 0
    });
      let result = await client
        .db('garageDB')
        .collection('charge')
        .insertOne(charge);
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

  const modifierCharge = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let amount = data.amount;
      let type = new TypeCharge(data.type);
      let result = await client
        .db('garageDB')
        .collection("charge")
        .updateOne({ _id: id }, { $set: { type : type , amount: amount , updatedAt : moment().format("DD/MM/YYYY HH:mm")} });
      res.status(200).json(result);
    } catch (error) {
      res.status(501).json(error);
    }
  };

  const validationPaiement = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let result = await client
        .db('garageDB')
        .collection("charge")
        .updateOne({ _id: id }, { $set: { payementState : 1 , updatedAt : moment().format("DD/MM/YYYY HH:mm") } });
      res.status(200).json(result);
    } catch (error) {
      res.status(501).json(error);
    }
  };

const getAllListeCharge = async (req, res) => {
    try {
        let result = await client.db("garageDB").collection("charge").find().sort({ createdAt : 1 }).toArray();
        res.status(200).json(result);
    } catch (error) {
        res.status(501).json(error);
    }
};

const getChargeParId = async (req, res) => {
    try {
        let id = new ObjectID(req.params.id);
        let result = await client.db("garageDB").collection("charge").find({ _id : id}).sort({ createdAt : 1 }).toArray();
        res.status(200).json(result);
    } catch (error) {
        res.status(501).json(error);
    }
};



module.exports = {
    ajoutCharge,
    modifierCharge,
    validationPaiement,
    getAllListeCharge,
    getChargeParId
};

