const { ObjectID } = require("bson");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const {getConnection, getDb} = require("../db/connect");
const tokenKey = 'azertyuiop';

const me = async(req, res) => {
    const client = getConnection();
    try {
        const userEntity = getDb(client,"garageDB").collection("users");
        const {authorization} = req.headers;
        const tokenParsed = authorization.split(" ");

        const {id} = jwt.verify(tokenParsed[1], tokenKey);
        const userFound = await userEntity.findOne(
            { _id: new ObjectID(id) },
            { projection: { accessToken: 0, password:0 } }
        );
        if (userFound) {
            return res.status(200).json(userFound);
        }
        return res.status(404).json(null);
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    } finally{
        client.close();
    }
}

const login = async (req,res) => {
    const client = getConnection();
    const userEntity = getDb(client,"garageDB").collection("users");
    const updatedAt = moment().format("DD/MM/YYYY HH:mm");
    const {email, password} = req.body;
    try {
        const userFound = await userEntity.findOne({email});
        if (userFound) {
            const validPassword = await bcrypt.compare(password, userFound.password);
            if (validPassword) {
                const token = jwt.sign({
                    _id: new ObjectID(userFound._id),
                    email: userFound.email,
                    profile: userFound.profile,
                    userRoles: userFound.usersRoles,
                }, tokenKey, { expiresIn: '23 hours' });
                const {value} = await userEntity.findOneAndUpdate({ _id: userFound._id }, { $set: { accessToken:token, updatedAt} }, {projection: {password: 0}})
                return res.status(200).json(value);
            }
            else{
                return res.status(403).json(null)
            }
        }
        return res.status(403).json(null)
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    } finally{
        client.close();
    }
}

module.exports={login,me}