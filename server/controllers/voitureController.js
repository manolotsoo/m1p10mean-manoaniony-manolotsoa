const { ObjectID } = require("bson");
const {getConnection,getDb} = require("../db/connect");
const moment = require('moment');
const { tokenKey } = require('../config');
const { Car } = require("../models/car");
const { DetReparation } = require("../models/detReparation");
const utils  = require("../models/utils");
const jwt = require('jsonwebtoken');

 /** ajout voiture */
  /*const ajouterVoiture = async (req, res) => {
    try {
      let data = req.body;
      let detReparationTab = Array();
      if(data.detReparation.length > 0){
        data.detReparation.forEach(liste => {
          let detRep = new DetReparation({
            name : liste.name,
            unitPrice : liste.unitPrice,
            itemNumber : liste.itemNumber,
            payementState : liste.payementState,
            createdAt : moment().format("DD/MM/YYYY HH:mm"),
            updatedAt : moment().format("DD/MM/YYYY HH:mm"),
            releaseAt : liste.releaseAt
          });
          detReparationTab.push(detRep);
        });
      }

      let voiture = new Voiture({
        id_user : data.id_user,
        model : data.model,
        mark: data.mark,
        matricule : data.matricule,
        createdAt : moment().format("DD/MM/YYYY HH:mm"),
        updatedAt : moment().format("DD/MM/YYYY HH:mm"),
        releaseAt : data.releaseAt,
        detReparation : detReparationTab,
    });
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .insertOne(voiture);
  
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };*/
  /*const getAllVoiture = async (req, res) => {
    try {
        let cursor = client
          .db("garageDB")
          .collection("voiture")
          .find()
          .sort({ nom: 1 });
        let result = await cursor.toArray();
        if (result.length > 0) {
          res.status(200).json(result);
        } else {
          res.status(204).json({ msg: "Aucun voiture trouvé" });
        }
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };*/
  
  /*const getVoitureById = async (req, res) => {
    try {
      let id = new ObjectID(req.params.id);
      let cursor = client.db("garageDB").collection("voiture").find({ _id: id });
      let result = await cursor.toArray();
      if (result.length > 0) {
        res.status(200).json(result[0]);
      } else {
        res.status(204).json({ msg: "Cet voiture n'existe pas" });
      }
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };*/
  
  /** modification des informations voiture */
  /*const updateVoiture = async (req, res) => {
    try {
        let id = new ObjectID(req.params.id);
        let data = req.body;
        let detReparationTab = Array();
        if(data.detReparation.length > 0){
          data.detReparation.forEach(liste => {
            let detRep = new DetReparation({
              name : liste.name,
              unitPrice : liste.unitPrice,
              itemNumber : liste.itemNumber,
              payementState : liste.payementState,
              createdAt : liste.createdAt,
              updatedAt : moment().format("DD/MM/YYYY HH:mm"),
              releaseAt : liste.releaseAt
            });
            detReparationTab.push(detRep);
          });
        };
        let model = data.model;
        let mark = data.mark;
        let matricule = data.matricule;
        let createdAt = data.createAt;
        let updatedAt = moment().format("DD/MM/YYYY HH:mm");
        let releaseAt =  data.releaseAt;
        let result = await client
        .db('garageDB')
        .collection("voiture")
        .updateOne({ _id: id }, { $set: { model : model, mark : mark , matricule:  matricule, profile : profile, createdAt : createdAt, updatedAt : updatedAt, releaseAt : releaseAt,detReparation : detReparationTab} });
  
      if (result.modifiedCount === 1) {
        res.status(200).json({ msg: "Modification réussie" });
      } else {
        res.status(404).json({ msg: "Cet voiture n'existe pas" });
      }
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };
/** ajout details reparations par voiture */
  /*const ajouterDetReparation = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let detReparationTab = Array();
      let listeDetRep = await client.db("garageDB").collection("voiture").find({ _id: id }).toArray();
      if(listeDetRep.length > 0)
          detReparationTab = listeDetRep[0].detReparation;
      if(data.detReparation.length > 0){
        data.detReparation.forEach(liste => {
          let detRep = new DetReparation({
            name : liste.name,
            unitPrice : liste.unitPrice,
            itemNumber : liste.itemNumber,
            payementState : liste.payementState,
            createdAt : moment().format("DD/MM/YYYY HH:mm"),
            updatedAt : moment().format("DD/MM/YYYY HH:mm"),
            releaseAt : liste.releaseAt
          });
          detReparationTab.push(detRep);
        });
      }
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .updateOne({ _id: id }, { $set: { detReparation : detReparationTab } });
  
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };
  
  const validationAvancementDetReparation = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let itemNumber = data.itemNumber;
      let detRepListe = Array();
      let car = await client
        .db('garageDB')
        .collection('voiture')
        .find({ _id: id }).toArray();
        if(car.length > 0){
          car.forEach(liste => {
            let detListe = liste.detReparation;
            if(detListe.length > 0){
              detListe.forEach(list => {
                  let detReparationValider;
                    if(list.itemNumber === itemNumber){
                      detReparationValider = new DetReparation({
                          name : list.name,
                          unitPrice : list.unitPrice,
                          itemNumber : list.itemNumber,
                          payementState : list.payementState,
                          createdAt : list.createdAt,
                          updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                          releaseAt : moment().format("DD/MM/YYYY HH:mm")
                      });
                    }
                    else{
                      detReparationValider = new DetReparation({
                          name : list.name,
                          unitPrice : list.unitPrice,
                          itemNumber : list.itemNumber,
                          payementState : list.payementState,
                          createdAt : list.createdAt,
                          updatedAt : list.updatedAt,
                          releaseAt : list.releaseAt
                      });
                    }
                  detRepListe.push(detReparationValider);
                });
            }
          });
        }
        let result = await client
            .db('garageDB')
            .collection('voiture')
            .updateOne({ _id: id }, { $set: { detReparation : detRepListe }});
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };
  

  const validationPaiement = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let itemNumber = data.itemNumber;
      let detRepListe = Array();
      let car = await client
        .db('garageDB')
        .collection('voiture')
        .find({ _id: id }).toArray();
        if(car.length > 0){
          car.forEach(liste => {
            let detListe = liste.detReparation;
            if(detListe.length > 0){
              detListe.forEach(list => {
                  let detReparationValider;
                    if(list.itemNumber === itemNumber){
                      detReparationValider = new DetReparation({
                          name : list.name,
                          unitPrice : list.unitPrice,
                          itemNumber : list.itemNumber,
                          payementState : 1,
                          createdAt : list.createdAt,
                          updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                          releaseAt : list.releaseAt
                      });
                    }
                    else{
                      detReparationValider = new DetReparation({
                          name : list.name,
                          unitPrice : list.unitPrice,
                          itemNumber : list.itemNumber,
                          payementState : list.payementState,
                          createdAt : list.createdAt,
                          updatedAt : list.updatedAt,
                          releaseAt : list.releaseAt
                      });
                    }
                  detRepListe.push(detReparationValider);
                });
            }
          });
        }
        let result = await client
            .db('garageDB')
            .collection('voiture')
            .updateOne({ _id: id }, { $set: { detReparation : detRepListe }});
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };
  
  
  */

  /** recherche par mark model matricule */
  const getVoiture = async (req, res) => {
    try {
      let liste = null;
      let name = req.params.rec;
      if(name === null || name === "")
        liste = await client.db("garageDB").collection("voiture").find().sort({ model: 1 }).toArray();
      else
        liste = await client.db("garageDB").collection("voiture").find({$or : [{mark : {$regex : name}},{model : {$regex : name}},{matricule : {$regex : name}}]}).toArray();
      let result = liste;
      if (result.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json({ msg: "Cet voiture n'existe pas" });
      }
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

  const getCar = async (req,res) => {
    const client = getConnection();
    const {id} = req.params;
    const data = req.body;
    const carEntity = getDb(client,"garageDB").collection("cars");
    try {
      const result = await carEntity.findOne({_id: new ObjectID(id)})
      if (!result) {
        return res.status(404).json(result);
      }
      return res.status(200).json(result);
    } catch (error) {
      return res.status(500).json(error);
    } finally{
      client.close();
    }
  }

  const receiveCar = async (req,res) => {
    const client = getConnection();
    const updatedAt = moment().format("DD/MM/YYYY HH:mm");
    const {authorization} = req.headers;
    // car ID
    const {id} = req.params;
    let tokenParsed = authorization ? authorization.split(" ") : "";
    if (!tokenParsed[1]) {
      throw new Error("Token unexpected");
    }
    const {_id} = jwt.verify(tokenParsed[1], tokenKey);
    const carEntity = getDb(client,"garageDB").collection("cars");
    try {
      const result = await carEntity.updateOne({ _id: new ObjectID(id) }, { $set: { receiver: _id, receive:updatedAt ,updatedAt} }).catch(error => {
        throw error;
      })
      return res.status(200).json(result);
    } catch (error) {
      return res.status(500).json(error);
    } finally{
      client.close();
    }
  }


  /** ajouter voiture */
  const addCar = async (req, res) => {

    const {authorization} = req.headers;
    let tokenParsed = authorization ? authorization.split(" ") : "";
    if (!tokenParsed[1]) {
      throw new Error("Token unexpected");
    }

    const userCustomer = jwt.verify(tokenParsed[1], tokenKey);
    delete userCustomer['iat'];
    delete userCustomer['exp'];
    delete userCustomer['accessToken'];
    const client = getConnection();
    const data = req.body;
    try {
      let voiture = new Car({
        model : data.model,
        mark: data.mark,
        user: new ObjectID(userCustomer._id),
        registrationNumber : data.registrationNumber,
        receive : moment().format("DD/MM/YYYY HH:mm"),
        createdAt : moment().format("DD/MM/YYYY HH:mm"),
        updatedAt : moment().format("DD/MM/YYYY HH:mm"),
    });
    const carEntity = getDb(client,"garageDB").collection("cars");

      let result = await carEntity
        .insertOne(voiture);
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    } finally{
      client.close();
    }
  };

  /** tous les voitures du current user */
  const getCarCurrentUser = async (req, res) => {
    const client = getConnection();
    const {authorization} = req.headers;
    let tokenParsed = authorization ? authorization.split(" ") : "";
    if (!tokenParsed[1]) {
      throw new Error("Token unexpected");
    }

    const userCustomer = jwt.verify(tokenParsed[1], tokenKey);
    delete userCustomer['iat'];
    delete userCustomer['exp'];
    delete userCustomer['accessToken'];

    const carEntity = getDb(client,"garageDB").collection("cars");
    try {
      const result = await carEntity.find({user:new ObjectID("63d77d23dbe73b0f3ac180f7")},{ projection: { user : 0 }}).toArray();
      return res.status(200).json(result);
    } catch (error) {
      return res.status(501).json(error);
    } finally{
      client.close();
    }
  }

  /** ajouter details reparation */
  const ajouterDetReparation = async (req, res) => {
    const client = getConnection();
    const {detReparation} = req.body;
    const carEntity = getDb(client,"garageDB").collection("cars");

    try {
      let id = new ObjectID(req.params.id);
      let detReparationTab = Array();
      let carFound = await carEntity.findOne({ _id: id });
      let currentListDetReparation = carFound.detReparation;
      if(currentListDetReparation && currentListDetReparation.length > 0){
          currentListDetReparation.forEach(reparation => {
            let detRep = new DetReparation({
            name : reparation.name,
            unitPrice : reparation.unitPrice,
            itemNumber : reparation.itemNumber,
            payementState : reparation.payementState,
            createdAt : moment().format("DD/MM/YYYY HH:mm"),
            updatedAt : moment().format("DD/MM/YYYY HH:mm"),
            repState : false
          });
          detReparationTab.push(detRep);
        });
      }
          let detRep = new DetReparation({
            name : detReparation.name,
            unitPrice : detReparation.unitPrice,
            itemNumber : detReparation.itemNumber,
            payementState : detReparation.payementState || false,
            createdAt : moment().format("DD/MM/YYYY HH:mm"),
            updatedAt : moment().format("DD/MM/YYYY HH:mm"),
            repState : false
          });
          detReparationTab.push(detRep);
      let result = await carEntity
        .updateOne({ _id: id }, { $set: { detReparation : detReparationTab } });
      return res.status(200).json(result);
    } catch (error) {
      return res.status(501).json(error);
    } finally {
      client.close();
    }
  };

  /** modification details reparation */
  const modifierDetReparation = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let itemNumber = data.detReparation.itemNumber;
      let detReparationTab = Array();
      let liste = await client.db("garageDB").collection("voiture").find({ _id: id }).toArray();
      if(liste.length > 0){
        let voiture = liste[0];
        if(voiture.detReparation.length > 0){
            voiture.detReparation.forEach(list => {
                if(list.itemNumber === itemNumber){
                  let detRep = new DetReparation({
                    name : data.detReparation.name,
                    unitPrice : list.unitPrice,
                    itemNumber : itemNumber,
                    payementState : list.payementState,
                    createdAt : list.createdAt,
                    updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                    repState : list.repState,
                    releaseAt : list.releaseAt
                  });
                  list = detRep;
                }
                detReparationTab.push(list);
            });
        }
      }
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .updateOne({ _id: id }, { $set: { detReparation : detReparationTab } });
  
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

  /** modification etat reparation */
  const modifierEtatReparation = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let itemNumber = data.itemNumber;
      let repState = data.repState;
      let detReparationTab = Array();
      let liste = await client.db("garageDB").collection("voiture").find({ _id: id }).toArray();
      if(liste.length > 0){
        let voiture = liste[0];
        if(voiture.detReparation.length > 0){
            voiture.detReparation.forEach(list => {
                if(list.itemNumber === itemNumber){
                  let detRep = new DetReparation({
                    name : list.name,
                    unitPrice : list.unitPrice,
                    itemNumber : list.itemNumber,
                    payementState : list.payementState,
                    createdAt : list.createdAt,
                    updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                    repState : repState,
                    releaseAt : list.releaseAt
                  });
                  list = detRep;
                }
                detReparationTab.push(list);
            });
        }
      }
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .updateOne({ _id: id }, { $set: { detReparation : detReparationTab } });
  
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };



  /** validation bon de sortie */
  const validationBonSortie = async (req, res) => {
    try {
      let id = new ObjectID(req.body.id);
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .updateOne({ _id: id }, { $set: { releaseAt : moment().format("DD/MM/YYYY HH:mm") } });
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

  
 /** liberation details reparation */
 const liberationDetReparation = async (req, res) => {
  try {
    let data = req.body;
    let id = new ObjectID(data.id);
    let itemNumber = data.itemNumber;
    let detRepListe = Array();
    let car = await client
      .db('garageDB')
      .collection('voiture')
      .find({ _id: id }).toArray();
      if(car.length > 0){
        let voiture = car[0];
        let detListe = voiture.detReparation;
        if(detListe.length > 0){
          detListe.forEach(list => {
            if(list.itemNumber === itemNumber){
              let detReparationValider = new DetReparation({
                  name : list.name,
                  unitPrice : list.unitPrice,
                  itemNumber : list.itemNumber,
                  payementState : list.payementState,
                  createdAt : list.createdAt,
                  updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                  releaseAt : moment().format("DD/MM/YYYY HH:mm"),
                  repState : "fait"
              });
              list = detReparationValider;
            }
            detRepListe.push(list);
          });
        }
      }
      let result = await client
          .db('garageDB')
          .collection('voiture')
          .updateOne({ _id: id }, { $set: { detReparation : detRepListe }});
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

  /** les des reparations avec montant et etat avancement */
  const getListeReparationMontantAndAvancement = async (req, res) => {
    try {
      let id = new ObjectID(req.params.id);
      let result = await client
        .db('garageDB')
        .collection('voiture')
        .find({ _id: id },{ projection: { detReparation : 1 }} ).toArray();
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

  /** validation des paiement */
  const validationPaimenent = async (req, res) => {
    try {
      let data = req.body;
      let id = new ObjectID(data.id);
      let itemNumber = data.itemNumber;
      let detRepListe = Array();
      let car = await client
        .db('garageDB')
        .collection('voiture')
        .find({ _id: id }).toArray();
        if(car.length > 0){
          let voiture = car[0];
          let detListe = voiture.detReparation;
          if(detListe.length > 0){
            detListe.forEach(list => {
              if(list.itemNumber === itemNumber){
                let detReparationValider = new DetReparation({
                    name : list.name,
                    unitPrice : list.unitPrice,
                    itemNumber : list.itemNumber,
                    payementState : 1,
                    createdAt : list.createdAt,
                    updatedAt : moment().format("DD/MM/YYYY HH:mm"),
                    releaseAt : list.releaseAt,
                    repState : list.repState
                });
                list = detReparationValider;
              }
              detRepListe.push(list);
            });
          }
        }
        let result = await client
            .db('garageDB')
            .collection('voiture')
            .updateOne({ _id: id }, { $set: { detReparation : detRepListe }});
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

const getFactureByItemNumberAndVoiture = async (req, res) => {
    try {
      let facture = { users : null , car : null};
      let data = req.params;
      let id = new ObjectID(data.id);
      let itemNumber = data.itemNumber;
      let car = await client
        .db('garageDB')
        .collection('voiture')
        .find({ _id: id }).toArray();
        if(car.length > 0){
          let liste = car[0];
          /** get users */
          let id_user = new ObjectID(liste.id_user);
          let users  = await client.db('garageDB').collection('users').find({ _id: id_user }).toArray();
          if(users.length > 0)
              facture.users = users[0];

          /** get details réparation */
          let detListe = liste.detReparation;
          if(detListe.length > 0){
            detListe.forEach(list => {
                let detReparationValider;
                  if(list.itemNumber === itemNumber){
                    detReparationValider = new DetReparation({
                        name : list.name,
                        unitPrice : list.unitPrice,
                        itemNumber : list.itemNumber,
                        payementState : list.payementState,
                        createdAt : list.createdAt,
                        updatedAt : list.updatedAt,
                        releaseAt : list.releaseAt
                    });
                    facture.car = detReparationValider  
                  }
              });
          }
        }
      res.status(200).json(facture);
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }
  };

/** stat */
  /** temps reparation moyenne de voiture par periode*/
          /*const tpsReparationMoyenneParPeriode = async (req, res) => {
            try {
              let periode = utils.getPeriode(req.params.periode);
              let tps = Array();
              let result = null;
              let listeVoiture = await client
                .db('garageDB')
                .collection('voiture')
                .find({ receive : {$regex : periode} } ).toArray();
              if(listeVoiture.length > 0){
                  listeVoiture.forEach(liste => {
                      if(liste.releaseAt !== null){
                          let tpsHeure = utils.dateDiffParHeure(liste.receive,liste.releaseAt);
                          tps.push(tpsHeure);
                      }
                  });
              }
              if(tps.length > 0){
                let sommeHeure = 0
                  tps.forEach(liste => {
                    sommeHeure += liste;
                  });
                result = sommeHeure / tps.length;
              }
              res.status(200).json(result);
            } catch (error) {
              console.log(error);
              res.status(501).json(error);
            }
          };

          const chiffreAffaireParPeriode = async (req, res) => {
            try {
              let periode = utils.getPeriode(req.params.periode);
              let montant = 0.0;
              let result = {
                periode : null,
                montantParMois : 0.0,
                montantParJours : 0.0
              };
              let listeVoiture = await client
                .db('garageDB')
                .collection('voiture')
                .find({ receive : {$regex : periode}}).toArray();
              if(listeVoiture.length > 0){
                  listeVoiture.forEach(liste => {
                      if(liste.releaseAt !== null){
                        let listeDetRep = liste.detReparation;
                          if(listeDetRep.length > 0){
                              listeDetRep.forEach(list => {
                                  if(list.payementState === 1){
                                    montant += parseFloat(list.unitPrice);
                                  }
                              }); 
                          }
                      }
                  });
              }
              result.periode = periode;
              result.montantParMois = montant;
              result.montantParJours = montant / 30;
              res.status(200).json(result);
            } catch (error) {
              console.log(error);
              res.status(501).json(error);
            }
          };*/

/** temps reparation moyenne de voiture */
const tpsReparationMoyenne = async (req, res) => {
  try {
    let tps = Array();
    let result = null;
    let listeVoiture = await client
      .db('garageDB')
      .collection('voiture')
      .find().toArray();
    if(listeVoiture.length > 0){
        listeVoiture.forEach(liste => {
            if(liste.releaseAt !== null){
                let tpsHeure = utils.dateDiffParHeure(liste.receive,liste.releaseAt);
                tps.push(tpsHeure);
            }
        });
    }
    if(tps.length > 0){
      let sommeHeure = 0
        tps.forEach(liste => {
          sommeHeure += liste;
        });
      result = sommeHeure / tps.length;
    }
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

/** chiffre d'affaire par periode */
/**/

/** chiffre d'affaire par mois et par jours */
const chiffreAffaireParMoisParJours = async (req, res) => {
  try {
    let result = Array();
    let listeVoiture = await client
      .db('garageDB')
      .collection('voiture')
      .find().toArray();
    if(listeVoiture.length > 0){
        let allPeriode = utils.getAllPeriode(listeVoiture);
        if(allPeriode.length > 0){
            for(let i = 0 ; i< allPeriode.length ; i++){
                let obj = { periode : null , montantParMois : 0.0 , montantParJours : 0.0 };
                obj.periode = allPeriode[0];
                let montant = 0.0;
                let liste = await client
                .db('garageDB')
                .collection('voiture')
                .find({ receive : {$regex : allPeriode[i]}}).toArray();
                if(liste.length > 0){
                  liste.forEach(element => {
                    if(element.releaseAt !== null){
                      let detRep = element.detReparation;
                      if(detRep.length > 0){
                        detRep.forEach(list => {
                            montant += parseFloat(list.unitPrice);
                        });
                      }
                    } 
                  });
                  obj.montantParMois = montant;
                  obj.montantParJours = montant / 30;
                }
                result.push(obj);
            }
        }
    }
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

/** benefice par mois  */

const beneficeParMois = async (req, res) => {
  try {
    let result = Array();
    let listeVoiture = await client
      .db('garageDB')
      .collection('voiture')
      .find().toArray();
    if(listeVoiture.length > 0){
        let allPeriode = utils.getAllPeriode(listeVoiture);
        if(allPeriode.length > 0){
          for(let i = 0 ; i< allPeriode.length ; i++){
              let obj = { periode : null , montantParMois : 0.0 };
              obj.periode = allPeriode[0];
              let montant = 0.0;

              /** liste voiture par periode */
              let liste = await client
                .db('garageDB')
                .collection('voiture')
                .find({ receive : {$regex : allPeriode[i]}}).toArray();
              if(liste.length > 0){
                liste.forEach(element => {
                  if(element.releaseAt !== null){
                    let detRep = element.detReparation;
                    if(detRep.length > 0){
                      detRep.forEach(list => {
                          montant += parseFloat(list.unitPrice);
                      });
                    }
                  } 
                });
              }
              
              /** liste charge par periode */
              let montantCharge = 0.0;
              let listeCharge = await client
                .db('garageDB')
                .collection('charge')
                .find({ createdAt : {$regex : allPeriode[i]}}).toArray();
              if(listeCharge.length > 0){
                  listeCharge.forEach(element => {
                      if(element.payementState > 0){
                          montantCharge += parseFloat(element.amount);
                      }
                  });
              }
              obj.montantParMois = parseFloat(montant) - parseFloat(montantCharge);
              result.push(obj);
          }
        } 
    }
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};



  module.exports = {
      addCar,
      getCarCurrentUser,
      getCar,
      receiveCar,
      getVoiture,
      ajouterDetReparation,
      validationBonSortie,
      liberationDetReparation,
      getListeReparationMontantAndAvancement,
      validationPaimenent,
      getFactureByItemNumberAndVoiture,
      modifierDetReparation,
      modifierEtatReparation,
      tpsReparationMoyenne,
      chiffreAffaireParMoisParJours,
      beneficeParMois
  };