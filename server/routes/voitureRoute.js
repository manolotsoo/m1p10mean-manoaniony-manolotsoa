const express = require("express");
const {
  addCar,
  getCarCurrentUser,
  getCar,
  receiveCar,
  getVoiture,
  ajouterDetReparation,
  validationBonSortie,
  liberationDetReparation,
  getListeReparationMontantAndAvancement,
  validationPaimenent,
  getFactureByItemNumberAndVoiture,
  modifierDetReparation,
  modifierEtatReparation,
  tpsReparationMoyenne,
  chiffreAffaireParMoisParJours,
  beneficeParMois
} = require("../controllers/voitureController");
const router = express.Router();

router.route("/add").post(addCar);
router.route("/getCarCurrentUser").get(getCarCurrentUser);
router.route("/getCar/:id").get(getCar);
router.route("/receive/:id").post(receiveCar);
router.route("/getRecVoiture/:rec").get(getVoiture);
router.route("/ajoutDetReparation/:id").post(ajouterDetReparation);
router.route("/validationBonSortie").post(validationBonSortie);
router.route("/releaseDetRep").post(liberationDetReparation);
router.route("/getDetailsRep/:id").get(getListeReparationMontantAndAvancement);
router.route("/paymentConfirm").post(validationPaimenent);
router.route("/getFacture/:id/:itemNumber").get(getFactureByItemNumberAndVoiture);
router.route("/updateDetRep").post(modifierDetReparation);
router.route("/updateRepState").post(modifierEtatReparation);
router.route("/getTpsRep").get(tpsReparationMoyenne);
router.route("/turnover").get(chiffreAffaireParMoisParJours);
router.route("/benefit").get(beneficeParMois);

module.exports = router;
