const express = require("express");
const {
    ajoutCharge,
    modifierCharge,
    validationPaiement,
    getAllListeCharge,
    getChargeParId
} = require("../controllers/chargeController");
const router = express.Router();



router.route("/addCharge").post(ajoutCharge);
router.route("/updateCharge").post(modifierCharge);
router.route("/validPayement").post(validationPaiement);
router.route("/getListeAllCharge").get(getAllListeCharge);
router.route("/getChargeParId/:id").get(getChargeParId);

module.exports = router;
